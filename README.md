# unibas_team_detection

In this repository there is the UniBas Team's Python source code about SciRoc Challenge 2021 for the Episode 2 "Italian Sign Language Interpretation (LIS) ", using the TIAGo robot.

# Prerequisites

- Ubuntu 18.04
- Docker (https://docs.docker.com/engine/install/ubuntu/)

# Usage

## 1. Clone this repositories:

Open the terminal and clone this repositories: 

    git clone https://gitlab.com/unibasteam/unibas_team_detection.git
    
    git clone https://github.com/pal-robotics/pal_docker_utils.git

## 2. Build Docker Image

Open the terminal in the folder where the Dockerfile is located and type the following command:

    sudo docker build -t detection .

## 3. Run Docker Image

Open **first** terminal in "*pal_docker_utils/scripts*" folder and type the following commands to view all Docker Images and run Docker container:
    
    sudo docker images 
    
    sudo ./pal_docker.sh -it detection

In same terminal type the following command to run the ROS launch file:
    
    roslaunch detection detection.launch

Open **second** terminal type the following commands to run the same Docker container:

    sudo docker container list
    
    sudo docker exec -it <CONTAINER ID> /bin/bash

In same terminal type the following command to run the Game Controller:

    rosrun detection sciroc_gc.py

Select "Interpretation" and click on "Bind".

NOTE: The robot IP is 127.0.0.1

In **third** terminal type the following commands to run the same Docker container:

    sudo docker container list

    sudo docker exec -it <CONTAINER ID> /bin/bash

In same terminal type the following command to run the rosbag:

    cd src/detection/src/dataset

    rosbag play <bag_name.bag>

# NOTE: For more details see the video [here](https://gitlab.com/unibasteam/unibas_team_detection/-/blob/main/images/acqua.mp4)
