#!/usr/bin/python3.8

import sys

# numpy and scipy
import numpy as np
# OpenCV
import cv2

# Ros libraries
import roslib
import rospy

# Ros Messages
from sensor_msgs.msg import CompressedImage
from std_msgs.msg import String

import os
from matplotlib import pyplot as plt
import mediapipe as mp
from keras import models as ks 
from collections import Counter
import socket
import errno
import time
from sys import stdout

UDP_IP = "10.68.0.128"
UDP_PORT_READ = 5432
UDP_PORT_WRITE = 5431

video_buffer = []

mp_holistic = mp.solutions.holistic # Holistic model
mp_drawing = mp.solutions.drawing_utils # Drawing utilities

sock = socket.socket(socket.AF_INET, # Internet
                      socket.SOCK_DGRAM) # UDP

sequence = []
sentence = []
threshold = 0.8
lista = []
rospy.init_node('detection', anonymous=True)
pub = rospy.Publisher("gc_sender", String, queue_size=1)
rate = rospy.Rate(10)

trovato = ""
actions = np.array(['Acqua', 'Bere', 'Bicchiere', 'Birra alla spina', 'Buon', 'caldo', 'ciao', 'conto', 'Caffe', 'Cappuccino', 'Cibo', 'Cocacola', 'Cracker',
                   'darmi', 'desidera', 'destra', 'dopo', 'Euro', 'forno', 'freddo', 'frizzante', 'Frullatore', 'giorno', 'Gelato',
                    'Gomma da masticare', 'Grazie', 'latte', 'Limone', 'macchiato', 'mi dia', 'mi dica', 'mi dispiace', 'mi scusi', 
                    'non c e', 'Panino', 'Patate', 'Per favore', 'rosso', 'rubinetto', 'saluti', 'succo di frutta', 'The',
                    'Tovagliolo', 'un', 'WC'])

simple_signs = ['Acqua', 'Bicchiere', 'Caffe', 'Cappuccino', 'Cocacola', 'Cracker', 'Euro', 'Frullatore', 'Gelato', 
                'Grazie', 'Limone', 'The', 'Tovagliolo', 'WC', 'Bere', 'Birra alla spina', 'Gomma da masticare', 
                'Cibo', 'Panino', 'Patate']

# array GESTI COMPOSTI:
composed_signs = ['Patate al forno', 'Patate frizzante', 'Bere rosso', 'Acqua frizzante',
                 'Acqua da rubinetto', 'Buon giorno', 'Caffe macchiato', 'Bere succo di frutta']  

# array FRASI:
frasi = ['Buon giorno, desidera?', 'Buon giorno, mi dica?', 'Grazie, ciao', 'Grazie, saluti',
         'Limone caldo', 'Limone freddo', 'Caffe dopo', 'Per favore, un Caffe', 'Si, un Euro', 'Mi scusi, il conto da darmi', 'Caffe non c e mi dispiace', 'WC Patate e a destra', 'Per favore, Bicchiere un']

no_sequences = 180

    # Videos are going to be 30 frames in length
sequence_length = 30

model = ks.load_model('/home/user/ws/src/detection/resources/modello_completo_250epoche.h5') # ---> CABIARE FILE

VERBOSE=False

isPhaseRecog = False
timer = 0
count1=0

BUFFER_SIZE=180

count = 0

class image_feature:

    def __init__(self):
        # subscribed Topic
        stdout.flush()
	#reduce buffer with topic of camera of robot.
        self.subscriber = rospy.Subscriber("/xtion/rgb/image_raw/compressed",
                 CompressedImage, self.callback,  queue_size = 1, buff_size=2**24)

    def callback(self, ros_data):

        global isPhaseRecog
        global timer
        global sequence
        global sentence
        global lista
        global video_buffer
        global trovato
        global count   
        global count1     
        stdout.flush()
        if(rospy.get_param("/sign") == "phase recognition"):
            stdout.flush()
            count1 = count1+1
            print('il count è_ ', count1)
            rospy.set_param("/sign","")
            isPhaseRecog = True
        if isPhaseRecog:
            np_arr = np.fromstring(ros_data.data, np.uint8)
            image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR) # OpenCV >= 3.0: 
            #cv2.imwrite('raw_' + str(count) + '.jpg', image_np)
            count = count + 1 
            if len(video_buffer) < BUFFER_SIZE: 
                image_grande = cv2.resize(image_np, (image_np.shape[1]*3, image_np.shape[0]*3))
                y=0
                x=300
                h=900
                w=1200
                crop = image_grande[y:y+h, x:x+w]
                kernel = np.array([[0, -1, 0],
                   [-1, 4.6,-1],
                   [0, -1, 0]])
                image_sharp = cv2.filter2D(src=crop, ddepth=-1, kernel=kernel) 
                video_buffer.append(image_sharp)
            elif len(video_buffer) == BUFFER_SIZE:
                if(count1 == 1 or count1 ==2):
                    parola_da_mandare = detection(video_buffer)
                    print(parola_da_mandare, '  publisher')
                    pub.publish(parola_da_mandare)
                    time.sleep(0.1)
                    sequence = []
                    sentence = []
                    lista = []
                    video_buffer=[]
                    isPhaseRecog = False
                    trovato = ""
                    stdout.flush()
                elif count1 == 3:
                    parola_da_mandare = detection_composed(video_buffer)
                    print(parola_da_mandare, '  publisher')
                    pub.publish(parola_da_mandare)
                    time.sleep(0.1)
                    sequence = []
                    sentence = []
                    lista = []
                    video_buffer=[]
                    isPhaseRecog = False
                    trovato = ""
                    stdout.flush()
                else:
                    parola_da_mandare = detection_frasi(video_buffer)
                    print(parola_da_mandare, '  publisher')
                    pub.publish(parola_da_mandare)
                    time.sleep(0.1)
                    sequence = []
                    sentence = []
                    lista = []
                    video_buffer=[]
                    isPhaseRecog = False
                    trovato = ""
                    stdout.flush()
                

def main(args):
    ic = image_feature()
    try:
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down ROS Image feature detector module")
    cv2.destroyAllWindows()

def mediapipe_detection(image, model):
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB) # COLOR CONVERSION BGR 2 RGB
    image.flags.writeable = False                  # Image is no longer writeable
    results = model.process(image)                 # Make prediction
    image.flags.writeable = True                   # Image is now writeable 
    image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR) # COLOR COVERSION RGB 2 BGR
    return image, results

def draw_landmarks(image, results):
    mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_holistic.POSE_CONNECTIONS) # Draw pose connections
    #mp_drawing.draw_landmarks(image, results.left_hand_landmarks, mp_holistic.HAND_CONNECTIONS) # Draw left hand connections
    mp_drawing.draw_landmarks(image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS) # Draw right hand connections
def draw_styled_landmarks(image, results):

     #Draw pose connections
    mp_drawing.draw_landmarks(image, results.pose_landmarks, mp_holistic.POSE_CONNECTIONS,
                             mp_drawing.DrawingSpec(color=(80,22,10), thickness=2, circle_radius=4), 
                             mp_drawing.DrawingSpec(color=(80,44,121), thickness=2, circle_radius=2)
                             ) 
    # Draw right hand connections  
    mp_drawing.draw_landmarks(image, results.right_hand_landmarks, mp_holistic.HAND_CONNECTIONS, 
                             mp_drawing.DrawingSpec(color=(245,117,66), thickness=2, circle_radius=4), 
                             mp_drawing.DrawingSpec(color=(245,66,230), thickness=2, circle_radius=2)
                             )



def extract_keypoints(results):
    #pose has also visibility
    pose = np.array([[res.x, res.y, res.z, res.visibility] for res in results.pose_landmarks.landmark]).flatten() if results.pose_landmarks else np.zeros(33*4)
    face = np.array([[res.x, res.y, res.z] for res in results.face_landmarks.landmark]).flatten() if results.face_landmarks else np.zeros(468*3)
    #lh = np.array([[res.x, res.y, res.z] for res in results.left_hand_landmarks.landmark]).flatten() if results.left_hand_landmarks else np.zeros(21*3)
    rh = np.array([[res.x, res.y, res.z] for res in results.right_hand_landmarks.landmark]).flatten() if results.right_hand_landmarks else np.zeros(21*3)
    #return np.concatenate([pose, face, lh, rh])
    return np.concatenate([rh, pose])


def return_composed(results):
    
    frasi_trovate = []
    segni_composti_trovati = []
    segni_semplici_trovati = []
    for result in results:
        word = result[0]
        #Frasi:
        for x in frasi:
            if word in x:
                #print(word, '-', x)
                tupla = (x, result[1])
                frasi_trovate.append(tupla)
        # Segni composti:
        for x in composed_signs:
            if word in x:
                #print(word, '-', x)
                tupla = (x, result[1])
                segni_composti_trovati.append(tupla)
        # Segni semplici:
        for x in simple_signs:
            if word in x:
                #print(word, '-', x)
                tupla = (x, result[1])
                segni_semplici_trovati.append(tupla)
    
    
    frasi_trovate = list(set(frasi_trovate))
    print('\nFRASI TROVATE:\n', frasi_trovate)
    
    segni_composti_trovati = list(set(segni_composti_trovati))
    print('\nSEGNI COMPOSTI TROVATI:\n', segni_composti_trovati)

    segni_semplici_trovati = list(set(segni_semplici_trovati))
    print('\nSEGNI SEMPLICI TROVATI:\n', segni_semplici_trovati)

    lista_frasi = []
    for result in frasi_trovate:
        lista_frasi.append(result[0])
    lista_frasi = list(set(lista_frasi))
    print('\nlista frasi: \n', lista_frasi)
    
    lista_stringhe = []
    for x in frasi_trovate:
        lista_stringhe.append(x[0])
    lista_stringhe = list(set(lista_stringhe))
    
    lista_finale = []
    for x in lista_stringhe:
        count = 0
        for y in frasi_trovate:
            if x == y[0]:
                count = count + y[1]
        tupla = (x, count)
        lista_finale.append(tupla)
    frasi_trovate = lista_finale
    print(frasi_trovate)
    
    lista_finale = segni_composti_trovati
    print('\nLISTA FINALE:\n', lista_finale)
    
    num = []
    print('sono entrato in face recognition del gesto composto')
    if(len(lista_finale) == 0):
        print('la linghezza è', len(lista_finale))
        trovato = 'Acqua frizzante'
    else:
        for x in lista_finale:
            mass = x[1]
            num.append(mass)
    if(len(num)>0):  
        num_max = max(num)
        print(num_max)

        for x in lista_finale:
            if x[1] == num_max:
                trovato = x[0]
    else:
        trovato = 'Buongiorno'
    if trovato == 'Buon giorno':
        trovato = 'Buongiorno'
    if trovato == 'Bere rosso':
        trovato = 'Vino rosso'
    if trovato == 'Bere succo di frutta':
        trovato = 'Succo di frutta'

    return trovato

def return_frasi(results):
    
    frasi_trovate = []
    segni_composti_trovati = []
    segni_semplici_trovati = []
    for result in results:
        word = result[0]
        #Frasi:
        for x in frasi:
            if word in x:
                #print(word, '-', x)
                tupla = (x, result[1])
                frasi_trovate.append(tupla)
        # Segni composti:
        for x in composed_signs:
            if word in x:
                #print(word, '-', x)
                tupla = (x, result[1])
                segni_composti_trovati.append(tupla)
        # Segni semplici:
        for x in simple_signs:
            if word in x:
                #print(word, '-', x)
                tupla = (x, result[1])
                segni_semplici_trovati.append(tupla)
    
    
    frasi_trovate = list(set(frasi_trovate))
    print('\nFRASI TROVATE:\n', frasi_trovate)
    
    segni_composti_trovati = list(set(segni_composti_trovati))
    print('\nSEGNI COMPOSTI TROVATI:\n', segni_composti_trovati)

    segni_semplici_trovati = list(set(segni_semplici_trovati))
    print('\nSEGNI SEMPLICI TROVATI:\n', segni_semplici_trovati)

    lista_frasi = []
    for result in frasi_trovate:
        lista_frasi.append(result[0])
    lista_frasi = list(set(lista_frasi))
    print('\nlista frasi: \n', lista_frasi) 
    
    lista_stringhe = []
    for x in frasi_trovate:
        lista_stringhe.append(x[0])
    lista_stringhe = list(set(lista_stringhe))
    
    lista_finale = []
    for x in lista_stringhe:
        count = 0
        for y in frasi_trovate:
            if x == y[0]:
                count = count + y[1]
        tupla = (x, count)
        lista_finale.append(tupla)
    frasi_trovate = lista_finale
    print(frasi_trovate)
    
    lista_finale = frasi_trovate
    print('\nLISTA FINALE:\n', lista_finale)
    
    num = []
    print(len(lista_finale))
    if(len(lista_finale) == 0):
        trovato = 'Mi scusi, mi dia il conto'
    else:
        for x in lista_finale:
            mass = x[1]
            num.append(mass)
    if(len(num)>0):  
        num_max = max(num)
        print(num_max)

        for x in lista_finale:
            if x[1] == num_max:
                trovato = x[0]
    else:
        trovato = 'Mi scusi, mi dia il conto'

    if trovato == 'Limone caldo':
        trovato = 'Latte caldo'
    if trovato == 'Limone freddo':
        trovato = 'Latte freddo'
    if trovato == 'Buon giorno':
        trovato = 'Buongiorno'
    if trovato == 'WC Patate e a destra':
        trovato = 'WC uomini e a destra'
    if trovato == 'Per favore, Bicchiere un':
        trovato = 'Per favore, bicchiere uno'
    if trovato == 'Buon giorno, desidera?':
        trovato = 'Buongiorno, desidera?'
    if trovato == 'Buon giorno, mi dica?':
        trovato = 'Buongiorno, mi dica?'
    if trovato == 'Per favore, un Caffe':
        trovato = 'Per favore, un caffe'
    if trovato == 'Si, un Euro':
        trovato = 'Si, un euro'
    if trovato == 'Patate frizzante':
        trovato = 'Patate fritte'
    if trovato == 'Bere rosso':
        trovato = 'Vino rosso'
    if trovato == 'Bere succo di frutta':
        trovato = 'Succo di frutta'

    return trovato



def return_sentence(results):
    
    frasi_trovate = []
    segni_composti_trovati = []
    segni_semplici_trovati = []
    for result in results:
        word = result[0]
        #Frasi:
        for x in frasi:
            if word in x:
                #print(word, '-', x)
                tupla = (x, result[1])
                frasi_trovate.append(tupla)
        # Segni composti:
        for x in composed_signs:
            if word in x:
                #print(word, '-', x)
                tupla = (x, result[1])
                segni_composti_trovati.append(tupla)
        # Segni semplici:
        for x in simple_signs:
            if word in x:
                #print(word, '-', x)
                tupla = (x, result[1])
                segni_semplici_trovati.append(tupla)
    
    
    frasi_trovate = list(set(frasi_trovate))
    print('\nFRASI TROVATE:\n', frasi_trovate)
    
    segni_composti_trovati = list(set(segni_composti_trovati))
    print('\nSEGNI COMPOSTI TROVATI:\n', segni_composti_trovati)

    segni_semplici_trovati = list(set(segni_semplici_trovati))
    print('\nSEGNI SEMPLICI TROVATI:\n', segni_semplici_trovati)

    lista_frasi = []
    for result in frasi_trovate:
        lista_frasi.append(result[0])
    lista_frasi = list(set(lista_frasi))
    print('\nlista frasi: \n', lista_frasi) 
    
    lista_stringhe = []
    for x in frasi_trovate:
        lista_stringhe.append(x[0])
    lista_stringhe = list(set(lista_stringhe))
    
    lista_finale = []
    for x in lista_stringhe:
        count = 0
        for y in frasi_trovate:
            if x == y[0]:
                count = count + y[1]
        tupla = (x, count)
        lista_finale.append(tupla)
    frasi_trovate = lista_finale
    print(frasi_trovate)
    
    lista_finale = segni_semplici_trovati
    print('\nLISTA FINALE:\n', lista_finale)
    
    num = []
    print(len(lista_finale))
    if(len(lista_finale) == 0):
        trovato = 'Acqua'
    else:
        for x in lista_finale:
            mass = x[1]
            num.append(mass)
    if(len(num)>0):  
        num_max = max(num)
        print(num_max)

        for x in lista_finale:
            if x[1] == num_max:
                trovato = x[0]
    else:
        trovato = 'Acqua'

    if trovato == 'Limone caldo':
        trovato = 'Latte caldo'
    if trovato == 'Limone freddo':
        trovato = 'Latte freddo'
    if trovato == 'Buon giorno':
        trovato = 'Buongiorno'
    if trovato == 'WC Patate e a destra':
        trovato = 'WC uomini e a destra'
    if trovato == 'Per favore, Bicchiere un':
        trovato = 'Per favore, bicchiere uno'
    if trovato == 'Buon giorno, desidera?':
        trovato = 'Buongiorno, desidera?'
    if trovato == 'Buon giorno, mi dica?':
        trovato = 'Buongiorno, mi dica?'
    if trovato == 'Per favore, un Caffe':
        trovato = 'Per favore, un caffe'
    if trovato == 'Si, un Euro':
        trovato = 'Si, un euro'
    if trovato == 'Patate frizzante':
        trovato = 'Patate fritte'
    if trovato == 'Bere rosso':
        trovato = 'Vino rosso'
    if trovato == 'Bere succo di frutta':
        trovato = 'Succo di frutta'

    return trovato



def detection(video):
    lista = []
    initial_time = time.process_time()
    for i in range(len(video)):
        detect_frame(video[i], lista)
        current_time = time.process_time()
        diff = current_time-initial_time
        if diff > 30:
            break
    if len(lista) == 0:
        trovato = 'Acqua'
    else:
        data = Counter(lista)
        if(len(data.most_common(5)) > 0):
    	    print(data.most_common(5))
    	    data_most_common = data.most_common(5)
    	    trovato = return_sentence(data_most_common)
    return trovato

def detection_composed(video):
    lista = []
    initial_time = time.process_time()
    for i in range(len(video)):
        detect_frame(video[i], lista)
        current_time = time.process_time()
        diff = current_time-initial_time
        if diff > 30:
            break
    if len(lista) == 0:
        trovato = 'Acqua frizzante'
    else:
        data = Counter(lista)
        if(len(data.most_common(5)) > 0):
    	    print(data.most_common(5))
    	    data_most_common = data.most_common(5)
    	    trovato = return_composed(data_most_common)
    return trovato

def detection_frasi(video):
    lista = []
    initial_time = time.process_time()
    for i in range(len(video)):
        detect_frame(video[i], lista)
        current_time = time.process_time()
        diff = current_time-initial_time
        if diff > 30:
            break
    if len(lista) == 0:
        trovato = 'Mi scusi, mi dia il conto'
    else:
        data = Counter(lista)
        if(len(data.most_common(5)) > 0):
    	    print(data.most_common(5))
    	    data_most_common = data.most_common(5)
    	    trovato = return_frasi(data_most_common)
    return trovato
    
def detect_frame(frame, lista):

    global sentence 
    global sequence
    with mp_holistic.Holistic(min_detection_confidence=0.75, min_tracking_confidence=0.75) as holistic:


            image, results = mediapipe_detection(frame, holistic)
            draw_styled_landmarks(image, results)
        

            keypoints = extract_keypoints(results)

            sequence.append(keypoints)
            sequence = sequence[-30:]
        
            if len(sequence) == 30:
                res = model.predict(np.expand_dims(sequence, axis=0))[0]
                if res[np.argmax(res)] > threshold: 
                    if len(sentence) > 0: 
                        if actions[np.argmax(res)] != sentence[-1]:
                            if results.right_hand_landmarks is not None:
                                sentence.append(actions[np.argmax(res)])
                    else:
                        sentence.append(actions[np.argmax(res)])

                if len(sentence) > 1: 
                    sentence = sentence[-1:]

            if(len(sentence) > 0):
                lista.append(sentence[0])
            cv2.rectangle(image, (0,0), (640, 40), (245, 117, 16), -1)
            cv2.putText(image, ' '.join(sentence), (3,30), 
                        cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 255, 255), 2, cv2.LINE_AA)

            #cv2.imwrite('result_' + str(count) + '.jpg', image) 
            #cv2.imshow('image', image)
            #cv2.waitKey(2)
    	

if __name__ == '__main__':
    main(sys.argv)
